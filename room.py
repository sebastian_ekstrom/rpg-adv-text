from abc import ABCMeta, abstractmethod

class RoomTemplate(object):
    __metaclass__ = ABCMeta
    
    STATUS_NORMAL = 0
    STATUS_EXIT = 1
    STATUS_GAMEOVER = 2
    
    def __init__(self, name, description):
        self.name = name
        self.desc = description
        
    @abstractmethod
    def enter(self, player):
        pass
        
    def print_description(self):
        print(self.desc)

class BasicRoom(RoomTemplate):
    
    def __init__(self, name, description, directions, adjacent_rooms):
        if len(directions) != len(adjacent_rooms):
            raise Exception('Number of directions to move and number of rooms to move to not equal')
        super().__init__(name, description)
        self.dirs = directions
        self.adj_rooms = adjacent_rooms
        self.dir_dict = {}
        for i in range(len(self.dirs)):
            self.dir_dict[self.dirs[i]] = self.adj_rooms[i]
        
    def enter(self, player):
        self.print_description()
        self.print_directions()
        return self.STATUS_NORMAL
        
    def print_directions(self):
        n_dir = len(self.dirs)
        output = 'Obvious exits are '
        for i in range(n_dir):
            if i == n_dir - 1:
                postfix = '.'
            elif i == n_dir - 2:
                postfix = ' and '
            else:
                postfix = ', '
            output += self.dirs[i] + postfix
        print(output)
        
    def move(self, direction):
        if direction in self.dir_dict:
            return self.dir_dict[direction]
        return None
        
class ExitRoom(RoomTemplate):
    def __init__(self, name, description):
        super().__init__(name, description)
        
    def enter(self, player):
        self.print_description()
        return self.STATUS_EXIT
        
class DeathRoom(RoomTemplate):
    def __init__(self, name, description):
        super().__init__(name, description)
        
    def enter(self, player):
        self.print_description()
        return self.STATUS_GAMEOVER

test_room = BasicRoom('test_room', 'This is definitely a room.', ['east', 'north', 'everywhere'], ['room1', 'room2', 'room3'])
