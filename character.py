import random

import weapon as w
import attack as a

class Character(object):
    def __init__(self, name, max_hp, max_mp, atk, df, mag, res, weapon = None, skills = []):
        self.name = name
        self.max_hp = max_hp
        self.hp = max_hp
        self.max_mp = max_mp
        self.mp = max_mp
        self.atk = atk
        self.df = df
        self.mag = mag
        self.res = res
        self.weapon = weapon
        self.skills = skills
                
    def to_string(self):
        return self.name + '\nHP: ' + str(self.hp) + '/' + str(self.max_hp) + '\nMP: ' + str(self.mp) + '/' + str(self.max_mp)
        
    def moves_to_string(self):
        result = '0: Focus\n'
        if self.weapon:
            result += '1: ' + self.weapon.name + '\n'
            for nbr, skill in enumerate(self.skills):
                result += str(nbr + 2) + ': ' + skill.to_string() + '\n'
        else:
            for nbr, skill in enumerate(self.skills):
                result += str(nbr + 1) + ': ' + skill.to_string() + '\n'
        return result.strip()
        
    def valid_moves(self):
        return [i for i in range(len(self.skills) + 1 + bool(self.weapon))]
        
    def use_move(self, index, target):
        if index == 0:
            self.focus()
        elif self.weapon:
            if index == 1:
                self.attack(target)
            else:
                self.use_skill(index-2, target)
        else:
            self.use_skill(index-1, target)
        
        
    def attack(self, target):
        if self.weapon:
            print(self.name + ' attacks ' + target.name + ' with ' + self.weapon.name + '!')
            self.weapon.use(self, target)
            return True
        return False
        
    def focus(self):
        restore = min(self.max_mp // 4, self.max_mp - self.mp)
        self.mp += restore
        print(self.name + ' focuses to restore ' + str(restore) + ' MP!')
        
    def can_use_skill(self, index):
        return index < len(self.skills) and self.mp >= self.skills[index].cost
        
    def get_skill(index):
        if len(self.skills) > index:
            return self.skills[index]
        raise Exception("Invalid skill index")
            
    def use_skill(self, index, target):
        if len(self.skills) > index:
            skill = self.skills[index]
            if self.can_use_skill(index):
                print(self.name + ' uses ' + skill.name + ' on ' + target.name + '!')
                skill.use(self, target)
                return True
            return False
        raise Exception("Invalid skill index")
            
    def hurt(self, dmg):
        if dmg <= 0:
            print(self.name + ' takes no damage!')
        elif dmg > self.hp:
            self.hp = 0
            print(self.name + ' takes ' + str(dmg) + ' damage!\n' + self.name + ' is defeated!')
        else:
            self.hp -= dmg
            print(self.name + ' takes ' + str(dmg) + ' damage!')
            
                
    def possible_moves(self):
        result = []
        if self.weapon:
            result.append(0)
        for i in range(len(self.skills)):
            if self.can_use_skill(i):
                result.append(i + 1)
        return result
        
    def random_attack(self, target):
        possible = self.possible_moves()
        if len(possible) > 0:
            action = possible[random.randint(0, len(possible) - 1)]
            if action == 0:
                if self.weapon:
                    self.attack(target)
            else:
                if self.can_use_skill(action - 1):
                    self.use_skill(action - 1, target)
        else:
            self.focus()
            

# predefined characters

john = Character('John', 120, 10, 18, 16, 23, 10, w.hammer, [a.windy_thing])
jade = Character('Jade', 100, 12, 3, 5, 30, 23, skills = [a.duck_storm, a.piano_rain])
