import character as c
import attack as a
import weapon as w
import enemy as e
import behaviour as b

import copy
import os

def auto_battle(c1,c2):
    print(c1.name + ' challenges ' + c2.name + ' to a battle!')
    while True:
        input()
        print(c1.to_string() + '\n\n' + c2.to_string())
        input()
        c1.random_attack(c2)
        if c2.hp == 0:
            return
        input()
        print(c1.to_string() + '\n\n' + c2.to_string())
        input()
        c2.random_attack(c1)
        if c1.hp == 0:
            return
        
        
def battle_basic(player, enemy):
    print(player.name + ' challenges ' + enemy.name + ' to a battle!')
    while True:
        input()
        print(player.to_string() + '\n\n' + enemy.to_string())
        player_action(player, enemy)
        if enemy.hp == 0:
            return
        input()
        print(player.to_string() + '\n\n' + enemy.to_string())
        input()
        enemy.random_attack(player)
        if player.hp == 0:
            return

def battle(player, enemies):
    enemies = [copy.deepcopy(e) for e in enemies]
    print(player.name + ' initiates battle!\n')
    while True:
        print_battle_status(player, enemies)
        input()
        player_move(player, enemies)
        if all_defeated(enemies):
            print(player.name + ' won the battle!')
            return True
        for e in enemies:
            if e.hp > 0:
                enemy_move(e, player)
        if player.hp == 0:
            print(player.name + ' lost the battle...')
            return False
            
            
def print_battle_status(player, enemies):
    print(player.to_string() + '\n')
    for e in enemies:
        if e.hp > 0:
            print(e.to_string())
            
def all_defeated(enemies):
    for e in enemies:
        if e.hp > 0:
            return False
    return True
        

def player_move(player, enemies):
    action, needs_target = get_player_action(player)
    if needs_target:
        target = get_player_target(player, enemies)
        os.system('clear')
        player.use_move(action, target)
    else:
        os.system('clear')
        player.use_move(action, None)
        
    
def get_player_action(player):
    print('Enter move (h for help):')
    while(True):
        move = input()
        if move == 'h':
            print(player.moves_to_string())
            continue
        elif move.isdigit():
            move = int(move)
            if move in player.valid_moves():
                if move == 0:
                    return 0, False
                if player.weapon:
                    if move == 1:
                        return 1, True
                    else:
                        if player.can_use_skill(move-2):
                            return move, True
                        else:
                            print('Not enough MP!')
                            continue
                else:
                    if player.can_use_skill(move-1):
                        return move, True
                    else:
                        print('Not enough MP!')
                        continue
        print("Invalid move!")

def get_player_target(player, enemies):
    if len(enemies) == 1:
        return enemies[0]
    print('Enter target (h for help):')
    while(True):
        target = input()
        if target == 'h':
            for n, e in enumerate(enemies):
                if e.hp > 0:
                    print(str(n) + ': ' + e.name)
            continue
        elif target.isdigit():
            target = int(target)
            if target in range(len(enemies)) and enemies[target].hp > 0:
                return enemies[target]
        print("Invalid target!")
            
            
            
def player_action(player, enemy):
    print('Enter move (h for help):')
    while(True):
        move = input()
        if move == 'h':
            print(player.moves_to_string())
        elif move.isdigit():
            move = int(move)
            if move in player.valid_moves():
                if move == 0:
                    player.focus()
                    break
                if player.weapon:
                    if move == 1:
                        player.attack(enemy)
                        break
                    else:
                        if player.can_use_skill(move-2):
                            player.use_skill(move-2, enemy)
                            break
                        else:
                            print('Not enough MP!')
                            continue
                else:
                    if player.can_use_skill(move-1):
                        player.use_skill(move-1, enemy)
                        break
                    else:
                        print('Not enough MP!')
                        continue
        print("Invalid move!")
        
        
                        
def enemy_move(enemy, player):
    move = enemy.get_next_move()
    enemy.use_move(move, player)
    

if __name__ == '__main__':
    c1 = c.john
    #c1.weapon = w.hammer
    #c1.skills = [a.windy_thing]

    c2 = c.jade
    #c2.weapon = w.ultimate_death_ray
    
    e1 = e.Enemy('Jade', 40, 3, 5, 30, 23, skills = [a.duck_storm, a.piano_rain, a.flatten])

    #auto_battle(c1, c2)
    battle(c1, [e1, e1])
