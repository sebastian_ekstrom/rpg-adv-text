import attack as atk

class Weapon(object):
    def __init__(self, name, attack):
        self.name = name
        self.attack = attack
        
    def use(self, user, target):
        self.attack.use(user, target)

hammer = Weapon('Hammer', atk.Phys_Atk('', 4, 0))
ultimate_death_ray = Weapon('Ultimate Death Ray', atk.Mag_Atk('', 9999, 0))
rubber_duck = Weapon('Rubber Duck', atk.Phys_Atk('', 1, 0))
