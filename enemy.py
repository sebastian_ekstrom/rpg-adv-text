import random
import character as c
import behaviour as b

class Enemy(c.Character):
    
    def __init__(self, name, max_hp, atk, df, mag, res, weapon = None, skills = [], behaviour = b.RandomBehaviour()):
        super().__init__(name, max_hp, 0, atk, df, mag, res, weapon, skills)
        self.behaviour = behaviour
        
    def to_string(self):
        return self.name + '\nHP: ' + str(self.hp) + '/' + str(self.max_hp)
        
    def get_next_move(self):
        return self.behaviour.next_move(self)
        
    def use_move(self, index, target):
        if self.weapon:
            if index == 1:
                self.attack(target)
            else:
                self.use_skill(index-1, target)
        else:
            self.use_skill(index, target)

    def use_skill(self, index, target):
        if len(self.skills) > index:
            skill = self.skills[index]
            print(self.name + ' uses ' + skill.name + ' on ' + target.name + '!')
            skill.use(self, target, True)
            return True
        raise Exception("Invalid skill index")
