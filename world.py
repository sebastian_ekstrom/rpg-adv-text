import os

class World(object):
    INVALID = -1
    HELP = 0
    MOVE = 1
    ACTIONS = {'help' : HELP, 'go' : MOVE}
    
    def __init__(self, room_list):
        self.rooms = {}
        for room in room_list:
            self.rooms[room.name] = room
        self.current_room = room_list[0]
        
    def change_rooms(self, direction):
        new_room = self.current_room.move(direction)
        if new_room is not None:
            os.system('clear')
            print('You go ' + direction + '.\n')
            self.enter_room(self.rooms[new_room])
        else:
            print(str(direction) + ' is not a direction you can go from here!')
            
    def enter_room(self, room):
        self.current_room = room
        status = room.enter(self.player)
        if status == room.STATUS_EXIT:
            self.win()
        elif status == room.STATUS_GAMEOVER:
            self.lose()
            
    def win(self):
        print('Congratulations! You have won!')
        exit()
        
    def lose(self):
        print('You are dead.')
        exit()
        
    def parse_command(self):
        command = input('> ')
        tokens = command.split(maxsplit = 1)
        action = tokens[0]
        if action in self.ACTIONS:
            action = self.ACTIONS[action]
            if len(tokens) > 1:
                target = tokens[1]
                return action, target
            return action, None
        else:
            return self.INVALID, None
            
        
    def start(self, player):
        self.player = player
        os.system('clear')
        self.enter_room(self.current_room)
        self.main_loop()
        
    def main_loop(self):
        while True:
            action, target = self.parse_command()
            if action == self.MOVE:
                self.change_rooms(target)
            else:
                print('You cannot do that right now.')
