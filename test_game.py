import world as w
import room as r
import character as c

rooms = []

entrance_desc = """You stand in the entrance hall of the castle. The doors are shut tight
behind you."""
entrance_dirs = ['east', 'north', 'down']
entrance_arooms = ['hallway1', 'maze', 'cellar']
entrance = r.BasicRoom('entrance', entrance_desc, entrance_dirs, entrance_arooms)
rooms.append(entrance)

maze_desc = """You find yourself in the middle of a maze. Passages wind  off in every
direction."""
maze_dirs = ['east', 'northeast', 'north', 'northwest',
             'west', 'southwest', 'south', 'southeast']
maze_arooms = ['maze', 'maze', 'maze', 'entrance', 'maze', 'maze', 'maze', 'maze']
maze = r.BasicRoom('maze', maze_desc, maze_dirs, maze_arooms)
rooms.append(maze)

hallway1_desc = """You walk along a dark hallway that stretches off into the distance.
Rows of identical doors line the walls, but they all seem to be locked."""
hallway1_dirs = ['east', 'west']
hallway1_arooms = ['hallway2', 'entrance']
hallway1 = r.BasicRoom('hallway1', hallway1_desc, hallway1_dirs, hallway1_arooms)
rooms.append(hallway1)

hallway2_desc = """You walk along a dark hallway that stretches off into the distance.
Rows of identical doors line the walls, but they all seem to be locked."""
hallway2_dirs = ['east', 'west']
hallway2_arooms = ['hallway3', 'entrance']
hallway2 = r.BasicRoom('hallway2', hallway2_desc, hallway2_dirs, hallway2_arooms)
rooms.append(hallway2)

hallway3_desc = """You walk along a dark hallway that stretches off into the distance.
Rows of identical doors line the walls, but they all seem to be locked."""
hallway3_dirs = ['east', 'west']
hallway3_arooms = ['hallway4', 'entrance']
hallway3 = r.BasicRoom('hallway3', hallway3_desc, hallway3_dirs, hallway3_arooms)
rooms.append(hallway3)

hallway4_desc ="""You walk along a dark hallway that stretches off into the distance.
Rows of identical doors line the walls, but they all seem to be locked."""
hallway4_dirs = ['east', 'west']
hallway4_arooms = ['exit', 'entrance']
hallway4 = r.BasicRoom('hallway4', hallway4_desc, hallway4_dirs, hallway4_arooms)
rooms.append(hallway4)

cellar_desc ="""You enter the cellar. It is very dark down here, and you hope that you
don't get eaten by something you don't see."""
cellar_dirs = ['up', 'down', 'south']
cellar_arooms = ['entrance', 'pit', 'maze']
cellar = r.BasicRoom('cellar', cellar_desc, cellar_dirs, cellar_arooms)
rooms.append(cellar)

pit_desc ="""You spot an interesting hole in the floor, and decide that jumping down
it would be a wonderful idea. Sadly, it is much deeper than you thought,
and the bottom is covered in sharp spikes."""
pit = r.DeathRoom('pit', pit_desc)
rooms.append(pit)

exit_desc = """After wandering the seemingly endless corridor for a while, you
eventually end up in front of a door leading back outside!"""
exit_ = r.ExitRoom('exit', exit_desc)
rooms.append(exit_)

game = w.World(rooms)
game.start(c.john)
